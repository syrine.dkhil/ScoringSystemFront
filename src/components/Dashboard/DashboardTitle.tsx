import { Typography } from "@mui/material";
import * as React from "react";


interface TitleProps {
  children?: React.ReactNode;
}

function Title1(props: TitleProps) {
  return (
    <Typography component="h2" variant="h6" color="primary" gutterBottom>
      {props.children}
    </Typography>
  );
}

export default Title1;
